package com.example.ketyapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.ketyapp.R

class MainActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}