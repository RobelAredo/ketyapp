package com.example.ketyapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.example.ketyapp.R
import com.example.ketyapp.viewmodel.KetyAppViewModel

class KetyFragment : Fragment() {
    val ketyViewModel by viewModels<KetyAppViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setContent {
                JumboTron()
                navBar()
                Column(
                    modifier = Modifier
                        .verticalScroll(rememberScrollState())
                        .padding(0.dp, 300.dp, 0.dp, 0.dp),
                    verticalArrangement = Arrangement.spacedBy(-100.dp)
                ) {
                    Card(this@KetyFragment, "Categories", categories, Color.White)
                    Card(this@KetyFragment, "Skin Types", skinTypes, Color.LightGray)
                    Card(this@KetyFragment, "Recommended", categories, Color.White)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ketyViewModel.getTest()
    }


    val categories = listOf(
        Triple(R.drawable.ic_trophy, 0xFFFFF9E3, "Rank"),
        Triple(R.drawable.fire, 0xFFFBEEE6, "Hot"),
        Triple(R.drawable.heart, 0xFFF5E9F4, "Loved"),
        Triple(R.drawable.secrets, 0xFFE8F0E7, "Secrets"),
        Triple(R.drawable.ic_trophy, 0xFFFFF9E3, "Trophy"),
        Triple(R.drawable.ic_trophy, 0xFFFFF9E3, "Trophy")
    )
    val skinTypes = listOf(
        Triple(R.drawable.normal_skin, 0xFFFCF1F5, "Normal"),
        Triple(R.drawable.dry_skin, 0xFFF4F7F6, "Dry"),
        Triple(R.drawable.oily_skin, 0xFFFBEEEA, "Oily"),
        Triple(R.drawable.combine_skin, 0xFFF1EEF9, "Combined"),
        Triple(R.drawable.normal_skin, 0xFFFCF1F5, "Normal"),
        Triple(R.drawable.normal_skin, 0xFFFCF1F5, "Normal")
    )

    @Composable
    fun IconList(fragment: Fragment, icons: List<Triple<Int, Long, String>>) {
        LazyRow(
            modifier = Modifier.padding(20.dp),
            horizontalArrangement = Arrangement.spacedBy(20.dp)
        ) {
            items(items = icons) { item ->
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Box(
                        Modifier
                            .size(70.dp)
                            .clip(RoundedCornerShape(10.dp))
                            .background(Color(item.second))
                            .clickable {
                                findNavController(fragment).navigate(
                                    KetyFragmentDirections.actionKetyFragmentToIconFragment2(
                                        item.third
                                    )
                                )
                            }) {
                        Icon(item.first)
                    }
                    Text(item.third, fontSize = 20.sp, textAlign = TextAlign.Center)
                }
            }
        }
    }

    @Composable
    fun navBar() {
        Box (modifier = Modifier.fillMaxWidth().padding(0.dp, 20.dp)) {
            Row(modifier = Modifier.width(360.dp),
                horizontalArrangement = Arrangement.SpaceBetween) {
                Icon(R.drawable.dna, 110f)
                Row (modifier = Modifier.width(100.dp),
                horizontalArrangement = Arrangement.SpaceBetween){
                    Icon(R.drawable.search, 60f)
                    Icon(R.drawable.shopping_cart, 75f)
                }
            }
        }
    }


    @Composable
    fun Icon(icon: Int, size: Float = 135f) {
        val vector = ImageVector.vectorResource(id = icon)
        val painter = rememberVectorPainter(image = vector)

        Canvas(modifier = Modifier.padding(10.dp)) {
            with(painter) {
                draw(Size(size,size))
            }
        }
    }

    @Preview
    @Composable
    fun JumboTron() {
        val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.walking_dog))
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color(0xDDFFC300))
        ) {
            LottieAnimation(
                composition,
                isPlaying = true,
                iterations = Int.MAX_VALUE,
                alignment = Alignment.TopCenter
            )
        }
    }

    @Composable
    fun BoxScope.Options(
        fragment: Fragment,
        title: String,
        options: List<Triple<Int, Long, String>>
    ) {
        Column(modifier = Modifier) {
            Text(
                title, fontSize = 20.sp, fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(20.dp, 20.dp, 0.dp, 0.dp)
            )
            IconList(fragment, icons = options)
        }
    }

    @Composable
    fun Card(
        fragment: Fragment,
        title: String,
        options: List<Triple<Int, Long, String>>,
        color: Color
    ) {
        Box(
            Modifier
                .fillMaxWidth()
                .height(300.dp)
                .clip(RoundedCornerShape(30.dp, 30.dp))
                .background(color)
        ) { Options(fragment, title, options) }
    }

//@Composable
//fun BoxScope.Post(fragment: Fragment, title: String, options: List<Triple<Int, Long, String>>, color: Color) {
//    Column(modifier = Modifier) {
//        Text(title, fontSize = 20.sp, fontWeight = FontWeight.Bold,
//            modifier = Modifier.padding(20.dp, 20.dp, 0.dp, 0.dp))
//        IconList(fragment, icons = options)
//    }
//}
//
//@Composable
//fun Card2(fragment: Fragment, title: String, options: List<Triple<Int, Long, String>>, color: Color) {
//    Box(
//        Modifier
//            .fillMaxWidth()
//            .height(300.dp)
//            .clip(RoundedCornerShape(30.dp, 30.dp))
//            .background(color)){ Options(fragment, title, options, color) }
//}
}