package com.example.ketyapp.model

import android.util.Log
import com.example.ketyapp.model.remote.KetyAppService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

object KetyAppRepo {
    val baseUrl = "https://www.thecocktaildb.com/"

    fun getInstance(): Retrofit {
        return Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    suspend fun test() = withContext(Dispatchers.IO) {
        val ketyAppService = getInstance().create(KetyAppService::class.java)
        GlobalScope.launch {
            val result = ketyAppService.getCategories()
            Log.d("LOGGER", "TEST IN REPO $ketyAppService")
            if (result != null)
            // Checking the results
            Log.d("ayush: ", result.toString())
        }
    }

}