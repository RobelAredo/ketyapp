package com.example.ketyapp.model.remote

import com.example.ketyapp.model.response.CategoryDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface KetyAppService {
    @GET("/api/json/v1/1/list.php")
    suspend fun getCategories(@Query("c") type: String = "list") : CategoryDTO

    fun getDrinksInCategory(catergory: String)

    fun getDrinkDetails(drinkId: Int)
}