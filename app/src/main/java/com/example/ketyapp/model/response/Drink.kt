package com.example.ketyapp.model.response


import com.google.gson.annotations.SerializedName

data class Drink(
    val strCategory: String
)