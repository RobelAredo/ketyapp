package com.example.ketyapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ketyapp.model.KetyAppRepo
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class KetyAppViewModel: ViewModel() {
    val repo = KetyAppRepo
    var _data = MutableLiveData<Any>()
    val data: LiveData<Any> get() = _data

    fun getTest(){
        viewModelScope.launch {
            Log.d("LOGGER", "ViewModel")
            val ketyAppService = repo.test()
        }
    }
}